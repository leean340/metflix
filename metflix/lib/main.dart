import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:get/get.dart';
import 'package:metflix/user_interface/ui1_home_page/home_page.dart';
import 'package:metflix/user_interface/ui2_movie_details/movie_details.dart';

import 'hive/favourite_movies_hive.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FavouriteMoviesHive().initHive();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
        builder: (context, orientation, deviceType) {
          return GetMaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Metflix',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            initialRoute: '/',
            getPages: [
              GetPage(name: '/', page: () => MyHomePage()),
              GetPage(name: '/movie_details', page: () => MovieDetails())
            ],
          );
        },
    );
  }
}
