import 'package:dio/dio.dart';

class HttpService {
  Future<dynamic> getRequest(String url, Map<String, dynamic> params) async {
    Response response;
    var result;

    try{
      response = await Dio().get(url, queryParameters: params);
      if(response.statusCode == 200) {
        result = response.data;
      } else {
        result = null;
      }

    }catch(_){
      result = null;
    }

    return result;
  }
}