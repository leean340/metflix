const String apiKey= "df14cfddc31aa7e174526865d5969336";

const String domain = "https://api.themoviedb.org/3";
const String imagePath = "https://image.tmdb.org/t/p/w500";

const String getTrendingMovie =  "$domain/trending/movie/day";
const String getPopularMovie = "$domain/movie/popular";
const String getTopRatedMovie = "$domain/movie/top_rated";
const String getMovieDetails = "$domain/movie/{movie_id}";
