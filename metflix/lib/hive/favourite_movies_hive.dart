import 'dart:io';

import 'package:hive/hive.dart';
import 'package:metflix/dao/favourite_movies_dao.dart';
import 'package:path_provider/path_provider.dart';

class FavouriteMoviesHive {

  String box = "MyFavouriteMoviesBox";

  Future<void> initHive() async {
    Directory path = await getApplicationDocumentsDirectory();
    Hive..init(path.path)..registerAdapter(FavouriteMoviesDAOAdapter());
    await Hive.openBox<FavouriteMoviesDAO>(box);
  }

  Future<int>addFavouriteMovies(FavouriteMoviesDAO favouriteMoviesDAO) async {
    var box = await Hive.openBox<FavouriteMoviesDAO>(this.box);

    int idOfInput = await box.add(favouriteMoviesDAO);

    // Hive.close();

    return idOfInput;
  }

  Future<void>deleteFavouriteMovies(int key) async {
    var box = await Hive.openBox<FavouriteMoviesDAO>(this.box);

    box.delete(key);

  }
}
