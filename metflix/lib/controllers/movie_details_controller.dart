import 'package:get/get.dart';
import 'package:metflix/dao/movie_dao.dart';
import 'package:metflix/Http/http_url.dart' as url;
import 'package:metflix/http/http_service.dart';

class MovieDetailsController extends GetxController{
  var isMovieDetailsLoading = true.obs;
  var isMovieDetailsError = false.obs;
  var movieDetails = <MovieDAO>[].obs;

  @override
  void onInit() {
    super.onInit();
    getMovieDetails(Get.arguments);
  }

  void getMovieDetails(String? id) async {
    isMovieDetailsLoading(true);
    Map<String, dynamic> params = {"api_key": url.apiKey};
    var movieDetailsMap = await HttpService().getRequest(Uri.encodeFull(url.getMovieDetails.replaceAll('{movie_id}', id!)), params);
    if(movieDetailsMap != null) {
      movieDetails.add(MovieDAO.fromSingleMap(movieDetailsMap));
    } else {
      isMovieDetailsError(true);
    }

    isMovieDetailsLoading(false);
  }
}