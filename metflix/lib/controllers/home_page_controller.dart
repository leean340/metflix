import 'package:get/get.dart';
import 'package:metflix/dao/movie_dao.dart';
import 'package:metflix/http/http_service.dart';
import 'package:metflix/Http/http_url.dart' as url;

class HomePageController extends GetxController {
  var isTrendingLoading = true.obs;
  var isTopRatedLoading = true.obs;
  var isPopularLoading = true.obs;

  var isTrendingError = false.obs;
  var isTopRatedError = false.obs;
  var isPopularError = false.obs;

  var trendingNowMovies = <MovieDAO>[].obs;
  var topRatedMovies = <MovieDAO>[].obs;
  var popularMovies = <MovieDAO>[].obs;

  @override
  void onInit() {
    super.onInit();
    fetchTrendingMovie();
    fetchPopularMovie();
    fetchTopRatedMovie();
  }

  void fetchTrendingMovie() async {
    isTrendingLoading(true);
    Map<String, dynamic> params = {"api_key": url.apiKey};
    var trendingResponse = await HttpService()
        .getRequest(Uri.encodeFull(url.getTrendingMovie), params);
    if (trendingResponse != null) {
      for (dynamic trend in trendingResponse['results']) {
        MovieDAO movieDAO = MovieDAO.fromMap(trend);
        trendingNowMovies.add(movieDAO);
      }
    } else {
      isTrendingError(true);
    }

    isTrendingLoading(false);
  }

  void fetchPopularMovie() async {
    isPopularLoading(true);
    Map<String, dynamic> params = {"api_key": url.apiKey};
    var popularResponse = await HttpService()
        .getRequest(Uri.encodeFull(url.getPopularMovie), params);

    if(popularResponse != null){
      for (dynamic popular in popularResponse['results']) {
        MovieDAO movieDAO = MovieDAO.fromMap(popular);
        popularMovies.add(movieDAO);
      }
    }else{
      isPopularError(true);
    }

    isPopularLoading(false);
  }

  void fetchTopRatedMovie() async {
    isTopRatedLoading(true);
    Map<String, dynamic> params = {"api_key": url.apiKey};
    var topRatedResponse = await HttpService()
        .getRequest(Uri.encodeFull(url.getTopRatedMovie), params);
    if (topRatedResponse != null) {
      for (dynamic topRated in topRatedResponse['results']) {
        MovieDAO movieDAO = MovieDAO.fromMap(topRated);
        topRatedMovies.add(movieDAO);
      }
    } else {
      isTopRatedError(true);
    }

    isTopRatedLoading(false);
  }
}
