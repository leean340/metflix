import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metflix/dao/favourite_movies_dao.dart';
import 'package:metflix/hive/favourite_movies_hive.dart';
import 'package:metflix/utils/utils_service.dart';
import 'package:sizer/sizer.dart';

class FavouriteButton extends StatelessWidget {
  final int? favouriteMovieId;
  final int? movieId;
  final String? posterPath;
  final String? movieTitle;

  const FavouriteButton({required this.favouriteMovieId, required this.movieId, required this.posterPath, required this.movieTitle, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (favouriteMovieId != -1) {
          FavouriteMoviesHive()
              .deleteFavouriteMovies(
              favouriteMovieId!);
          UtilsService().showSnackBar(
              context,
              "Movie removed from my list.");
        } else {
          FavouriteMoviesDAO
          favouriteMoviesDAO =
          FavouriteMoviesDAO(
              id: movieId!,
              imageUrl:posterPath!,
              movieName: movieTitle!);
          FavouriteMoviesHive()
              .addFavouriteMovies(
              favouriteMoviesDAO);
          UtilsService().showSnackBar(
              context,
              "Movie added to my list.");
        }
      },
      child: Column(
        mainAxisAlignment:
        MainAxisAlignment.center,
        children: [
          Icon(
            favouriteMovieId == -1
                ? Icons.add
                : Icons.check,
            color: Colors.white,
            size: 3.h,
          ),
          Text(
            'My List',
            style: TextStyle(
                fontSize: 10.sp,
                color: Colors.white),
          )
        ],
      ),
    );
  }
}