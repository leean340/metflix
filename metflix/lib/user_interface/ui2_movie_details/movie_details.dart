import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:metflix/controllers/movie_details_controller.dart';
import 'package:metflix/dao/favourite_movies_dao.dart';
import 'package:metflix/extra_widget/favourite_button.dart';
import 'package:metflix/hive/favourite_movies_hive.dart';
import 'package:metflix/utils/utils_service.dart';
import 'package:sizer/sizer.dart';
import 'package:get/get.dart';
import 'package:metflix/Http/http_url.dart' as url;

class MovieDetails extends StatelessWidget {
  MovieDetails({Key? key}) : super(key: key);

  final movieDetailsController = Get.put(MovieDetailsController());
  final Box<FavouriteMoviesDAO>? favouriteMoviesBox =
      Hive.box<FavouriteMoviesDAO>(FavouriteMoviesHive().box);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ValueListenableBuilder(
          valueListenable: favouriteMoviesBox!.listenable(),
          builder:
              (context, Box<FavouriteMoviesDAO> favouriteMoviesDAOList, _) {
            int? favouriteMovieId = -1;

            return GetX<MovieDetailsController>(builder: (controller) {
              List<dynamic> genres = [];
              if (controller.movieDetails.isNotEmpty) {
                favouriteMovieId = favouriteMoviesDAOList.keys
                    .cast<int>()
                    .firstWhere(
                        (key) =>
                            favouriteMoviesDAOList.get(key)!.id.toString() ==
                            Get.arguments,
                        orElse: () => -1);

                genres = controller.movieDetails[0].genres;
              }

              return Scaffold(
                body: Container(
                  width: 100.w,
                  height: 100.h,
                  color: Colors.black,
                  child: controller.isMovieDetailsError.value
                      ? UtilsService().returnErrorWidget()
                      : Column(
                          children: [
                            SizedBox(
                              height: 30.h,
                              child: controller.movieDetails.isEmpty
                                  ? const Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : CachedNetworkImage(
                                      imageUrl:
                                          "${url.imagePath}${controller.movieDetails[0].backdropPath}",
                                      fit: BoxFit.contain,
                                      height: 30.h,
                                    ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 3.w),
                                child: SizedBox(
                                  width: 100.w,
                                  child: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 1.h),
                                          child: Text(
                                            controller.movieDetails.isNotEmpty
                                                ? "${controller.movieDetails[0].title}"
                                                : "",
                                            style: titleTextStyle,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 1.h),
                                          child: Row(
                                            children: [
                                              Text(
                                                controller
                                                        .movieDetails.isNotEmpty
                                                    ? "${controller.movieDetails[0].releaseDate}"
                                                    : "",
                                                style: dateTextStyle,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              SizedBox(
                                                width: 2.w,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: controller
                                                          .movieDetails.isEmpty
                                                      ? Colors.transparent
                                                      : Colors.grey,
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                ),
                                                child: Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 1.w),
                                                  child: Text(
                                                    controller.movieDetails
                                                            .isEmpty
                                                        ? ""
                                                        : controller
                                                                .movieDetails[0]
                                                                .adult
                                                            ? "18+"
                                                            : "12+",
                                                    style: ageTextStyle,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 2.w,
                                              ),
                                              Text(
                                                controller
                                                        .movieDetails.isNotEmpty
                                                    ? "${controller.movieDetails[0].runtime}m"
                                                    : "",
                                                style: runtimeTextStyle,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                            margin: EdgeInsets.only(top: 1.h),
                                            child: controller
                                                    .movieDetails.isEmpty
                                                ? Container()
                                                : RichText(
                                                    textAlign: TextAlign.left,
                                                    text: TextSpan(
                                                      children: <TextSpan>[
                                                        ...genres
                                                            .asMap()
                                                            .entries
                                                            .map((e) {
                                                          return TextSpan(
                                                              text:
                                                                  '${genres[e.key]['name']} ${e.key == genres.length - 1 ? "" : "•"} ',
                                                              style:
                                                                  genresTextStyle);
                                                        }),
                                                      ],
                                                    ),
                                                  )),
                                        Center(
                                          child: GestureDetector(
                                            onTap: () {
                                              UtilsService().showSnackBar(
                                                  context, "Coming soon!");
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(top: 1.h),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                              ),
                                              width: 100.w,
                                              height: 4.h,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Icon(
                                                    Icons.play_arrow,
                                                    color: Colors.black,
                                                    size: 3.h,
                                                  ),
                                                  SizedBox(
                                                    width: 3.w,
                                                  ),
                                                  Text(
                                                    "Play",
                                                    style: playButtonTextStyle,
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        Center(
                                          child: GestureDetector(
                                            onTap: () {
                                              UtilsService().showSnackBar(
                                                  context, "Coming soon!");
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(top: 1.h),
                                              decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.5),
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                              ),
                                              width: 100.w,
                                              height: 4.h,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Icon(
                                                    Icons.file_download,
                                                    color: Colors.white,
                                                    size: 3.h,
                                                  ),
                                                  SizedBox(
                                                    width: 3.w,
                                                  ),
                                                  Text(
                                                    "Download",
                                                    style:
                                                        downloadButtonTextStyle,
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 100.w,
                                          margin: EdgeInsets.only(top: 1.h),
                                          child: Text(
                                            controller.movieDetails.isNotEmpty
                                                ? "${controller.movieDetails[0].overview}"
                                                : "",
                                            style: overviewTextStyle,
                                            // overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                          width: 100.w,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Expanded(
                                                child: FavouriteButton(
                                                  favouriteMovieId:
                                                      favouriteMovieId!,
                                                  movieId: int.parse(
                                                      Get.arguments.toString()),
                                                  posterPath: controller
                                                          .movieDetails.isEmpty
                                                      ? ''
                                                      : '${controller.movieDetails[0].posterPath}',
                                                  movieTitle: controller
                                                          .movieDetails.isEmpty
                                                      ? ''
                                                      : controller
                                                          .movieDetails[0].title
                                                          .toString(),
                                                ),
                                              ),
                                              Expanded(
                                                  child: GestureDetector(
                                                onTap: () {
                                                  UtilsService().showSnackBar(
                                                      context, "Coming soon!");
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons
                                                          .thumb_up_alt_outlined,
                                                      color: Colors.white,
                                                      size: 3.h,
                                                    ),
                                                    Text(
                                                      'Rate',
                                                      style: TextStyle(
                                                          fontSize: 10.sp,
                                                          color: Colors.white),
                                                    )
                                                  ],
                                                ),
                                              )),
                                              Expanded(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    UtilsService().showSnackBar(
                                                        context,
                                                        "Coming soon!");
                                                  },
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Icon(
                                                        Icons.share,
                                                        color: Colors.white,
                                                        size: 3.h,
                                                      ),
                                                      Text(
                                                        'Share',
                                                        style: TextStyle(
                                                            fontSize: 10.sp,
                                                            color:
                                                                Colors.white),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                ), // This trailing comma makes auto-formatting nicer for build methods.
              );
            });
          }),
    );
  }
}

final TextStyle titleTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 18.sp,
);

final TextStyle dateTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 10.sp,
);

final TextStyle ageTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 9.sp,
);

final TextStyle runtimeTextStyle = TextStyle(
  color: Colors.white.withOpacity(0.6),
  fontWeight: FontWeight.bold,
  fontSize: 9.sp,
);

final TextStyle genresTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 10.sp,
);

final TextStyle playButtonTextStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
  fontSize: 15.sp,
);

final TextStyle downloadButtonTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 15.sp,
);

final TextStyle overviewTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 11.sp,
);
