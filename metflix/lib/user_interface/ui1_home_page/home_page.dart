import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:metflix/controllers/home_page_controller.dart';
import 'package:metflix/dao/favourite_movies_dao.dart';
import 'package:metflix/extra_widget/favourite_button.dart';
import 'package:metflix/hive/favourite_movies_hive.dart';
import 'package:metflix/user_interface/ui2_movie_details/movie_details.dart';
import 'package:metflix/utils/utils_service.dart';
import 'package:sizer/sizer.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:get/get.dart';
import 'package:metflix/Http/http_url.dart' as url;

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  final Box<FavouriteMoviesDAO>? favouriteMoviesBox =
      Hive.box<FavouriteMoviesDAO>(FavouriteMoviesHive().box);
  final homePageController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ValueListenableBuilder(
          valueListenable: favouriteMoviesBox!.listenable(),
          builder:
              (context, Box<FavouriteMoviesDAO> favouriteMoviesDAOList, _) {
            List<int> keys = favouriteMoviesDAOList.keys.cast<int>().toList();
            int? favouriteMovieId;

            return GetX<HomePageController>(builder: (controller) {
              int rand = 0;
              if (controller.trendingNowMovies.isNotEmpty) {
                final _random = Random();
                int next(int min, int max) => min + _random.nextInt(max - min);
                rand = next(1, controller.trendingNowMovies.length);
                favouriteMovieId = favouriteMoviesDAOList.keys
                    .cast<int>()
                    .firstWhere(
                        (key) =>
                            favouriteMoviesDAOList.get(key)!.id.toString() ==
                            controller.trendingNowMovies[rand].id,
                        orElse: () => -1);
              }

              return Scaffold(
                body: Container(
                  color: Colors.black,
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverAppBar(
                        expandedHeight: 60.h,
                        floating: false,
                        pinned: true,
                        snap: false,
                        flexibleSpace: FlexibleSpaceBar(
                          background: controller.trendingNowMovies.isEmpty
                              ? Container()
                              : Stack(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        Get.toNamed('/movie_details',
                                            arguments: controller
                                                .trendingNowMovies[rand].id);
                                      },
                                      child: SizedBox(
                                        width: 100.w,
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              "${url.imagePath}${controller.trendingNowMovies[rand].posterPath}",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        height: 8.h,
                                        width: 100.w,
                                        color: Colors.black.withOpacity(0.7),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Expanded(
                                              child: FavouriteButton(
                                                favouriteMovieId:
                                                    favouriteMovieId!,
                                                movieId: int.parse(controller
                                                    .trendingNowMovies[rand].id
                                                    .toString()),
                                                posterPath:
                                                    '${controller.trendingNowMovies[rand].posterPath}',
                                                movieTitle: controller
                                                    .trendingNowMovies[rand]
                                                    .title
                                                    .toString(),
                                              ),
                                            ),
                                            Expanded(
                                                child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 2.w),
                                              child: GestureDetector(
                                                onTap: () {
                                                  Get.toNamed('/movie_details',
                                                      arguments: controller
                                                          .trendingNowMovies[
                                                              rand]
                                                          .id);
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                  ),
                                                  height: 4.h,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Icon(
                                                        Icons.play_arrow,
                                                        color: Colors.black,
                                                        size: 3.h,
                                                      ),
                                                      SizedBox(
                                                        width: 3.w,
                                                      ),
                                                      Text(
                                                        "Play",
                                                        style:
                                                            playButtonTextStyle,
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            )),
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  Get.toNamed('/movie_details',
                                                      arguments: controller
                                                          .trendingNowMovies[
                                                              rand]
                                                          .id);
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons.info_outline,
                                                      color: Colors.white,
                                                      size: 3.h,
                                                    ),
                                                    Text(
                                                      'Info',
                                                      style: TextStyle(
                                                          fontSize: 10.sp,
                                                          color: Colors.white),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                        ),
                        backgroundColor: Colors.black,
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'M',
                              style: logoTextStyle,
                            ),
                            GestureDetector(
                              onTap: () {
                                UtilsService()
                                    .showSnackBar(context, "Coming soon!");
                              },
                              child: SizedBox(
                                height: 4.h,
                                width: 4.h,
                                child: Icon(
                                  Icons.search,
                                  color: Colors.white,
                                  size: 3.h,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      movieList(
                          "Trending Now",
                          controller.trendingNowMovies,
                          controller.isTrendingLoading.value,
                          controller.isTrendingError.value),
                      movieList(
                          "Top Rated",
                          controller.topRatedMovies,
                          controller.isTopRatedLoading.value,
                          controller.isTopRatedError.value),
                      movieList(
                          "Popular on Metflix",
                          controller.popularMovies,
                          controller.isPopularLoading.value,
                          controller.isPopularError.value),
                      favouriteMoviesList(
                          "My List", keys, favouriteMoviesDAOList)
                    ],
                  ),
                ),
              );
            });
          }),
    );
  }

  Widget movieList(
      String title, dynamic currentMovieList, bool isLoading, bool isError) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.only(top: 10),
          height: 30.h,
          width: 100.w,
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    title,
                    style: titleTextStyle,
                  )),
              Expanded(
                child: isError
                    ? Center(
                        child: UtilsService().returnErrorWidget(),
                      )
                    : isLoading
                        ? const Center(child: CircularProgressIndicator())
                        : ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: currentMovieList.length,
                            itemBuilder: (context, i) {
                              return GestureDetector(
                                onTap: () {
                                  Get.toNamed('/movie_details',
                                      arguments: currentMovieList[i].id);
                                },
                                child: Center(
                                  child: Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    width: 35.w,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: CachedNetworkImage(
                                            imageUrl:
                                                '${url.imagePath}${currentMovieList[i].posterPath}',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 2.w),
                                          child: Text(
                                            "${currentMovieList[i].title}",
                                            style: movieNameTextStyle,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
              ),
            ],
          ),
        );
      }, childCount: 1),
    );
  }

  Widget favouriteMoviesList(String title, List<int> keys,
      Box<FavouriteMoviesDAO> favouriteMoviesDAOList) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.only(top: 10),
          height: 30.h,
          width: 100.w,
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    title,
                    style: titleTextStyle,
                  )),
              Expanded(
                child: favouriteMoviesDAOList.length == 0
                    ? Center(
                        child: Text(
                        "Empty",
                        style: noDataTextStyle,
                      ))
                    : ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: favouriteMoviesDAOList.length,
                        itemBuilder: (context, i) {
                          final int key = keys[i];
                          final FavouriteMoviesDAO? favouriteMoviesDAO =
                              favouriteMoviesDAOList.get(key);

                          return GestureDetector(
                            onTap: () {
                              Get.toNamed('/movie_details',
                                  arguments: favouriteMoviesDAO!.id.toString());
                            },
                            child: Center(
                              child: Container(
                                margin: const EdgeInsets.only(left: 10),
                                width: 35.w,
                                child: Column(
                                  children: [
                                    Expanded(
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            '${url.imagePath}${favouriteMoviesDAO!.imageUrl}',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: Text(
                                        favouriteMoviesDAO.movieName,
                                        style: movieNameTextStyle,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
              ),
            ],
          ),
        );
      }, childCount: 1),
    );
  }
}

final TextStyle logoTextStyle = TextStyle(
  color: Colors.red,
  fontWeight: FontWeight.bold,
  fontSize: 25.sp,
  fontFamily: "LobsterRegular",
);

final TextStyle titleTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 20.sp,
);

final TextStyle movieNameTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 10.sp,
);

final TextStyle noDataTextStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 12.sp,
);

final TextStyle playButtonTextStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
  fontSize: 10.sp,
);
