class MovieDAO {
   String? posterPath;
   late bool adult;
   String? overview;
   String? releaseDate;
   dynamic genreIds;
   dynamic genres;
   String? id;
   String? originalTitle;
   String? originalLanguage;
   String? title;
   String? backdropPath;
   String? popularity;
   String? voteCount;
   bool? video;
   String? voteAverage;
   String? runtime;

  MovieDAO(
      {this.posterPath,
      required this.adult,
      this.overview,
      this.releaseDate,
      this.genreIds,
      this.id,
      this.originalTitle,
      this.originalLanguage,
      this.title,
      this.backdropPath,
      this.popularity,
      this.voteCount,
      this.video,
      this.voteAverage});

  MovieDAO.fromMap(Map<String, dynamic> map) {
    posterPath = "${map['poster_path']}";
    adult = map['adult'] == null ? false : true;
    overview = "${map['overview']}";
    releaseDate = "${map['release_date']}";
    genreIds = map['genre_ids'];
    id = "${map['id']}";
    originalTitle = "${map['original_title']}";
    originalLanguage = "${map['original_language']}";
    title = "${map['title']}";
    backdropPath = "${map['backdrop_path']}";
    popularity = "${map['popularity']}";
    voteCount = "${map['vote_count']}";
    video = map['video'];
    voteAverage = "${map['vote_average']}";
  }

   MovieDAO.fromSingleMap(Map<String, dynamic> map) {
     posterPath = "${map['poster_path']}";
     adult = map['adult'];
     overview = "${map['overview']}";
     releaseDate = "${map['release_date']}";
     genres = map["genres"];
     originalTitle = "${map['original_title']}";
     originalLanguage = "${map['original_language']}";
     title = "${map['title']}";
     backdropPath = "${map['backdrop_path']}";
     popularity = "${map['popularity']}";
     voteCount = "${map['vote_count']}";
     video = map['video'];
     voteAverage = "${map['vote_average']}";
     runtime = "${map['runtime']}";
   }
}
