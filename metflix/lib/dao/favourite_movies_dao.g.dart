// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favourite_movies_dao.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FavouriteMoviesDAOAdapter extends TypeAdapter<FavouriteMoviesDAO> {
  @override
  final int typeId = 0;

  @override
  FavouriteMoviesDAO read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FavouriteMoviesDAO(
      id: fields[0] as int,
      imageUrl: fields[1] as String,
      movieName: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, FavouriteMoviesDAO obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.imageUrl)
      ..writeByte(2)
      ..write(obj.movieName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FavouriteMoviesDAOAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
