import 'package:hive/hive.dart';

part 'favourite_movies_dao.g.dart';

@HiveType(typeId: 0)
class FavouriteMoviesDAO extends HiveObject {

  @HiveField(0)
  int id;

  @HiveField(1)
  String imageUrl;

  @HiveField(2)
  String movieName;

  FavouriteMoviesDAO(
      {required this.id, required this.imageUrl, required this.movieName});
}
