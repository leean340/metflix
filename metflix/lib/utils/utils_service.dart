import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class UtilsService {
  void showSnackBar(BuildContext context, String text) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(text),
    ));
  }

  Widget returnErrorWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.error, color: Colors.white, size: 3.h,),
        SizedBox(height: 2.h,),
        Text("Something went wrong, please try again later", style: TextStyle(fontSize: 10.sp, color: Colors.white),)
      ],
    );
  }
}
